#include <Joystick.h>
#include <Keyboard.h>


Joystick_ Joystick(JOYSTICK_DEFAULT_REPORT_ID,JOYSTICK_TYPE_GAMEPAD,
  11, 0,                  // Button Count, Hat Switch Count
  true, true, false,     // X and Y, but no Z Axis
  false, false, false,   // No Rx, Ry, or Rz
  false, false,          // No rudder or throttle
  false, false, false);  // No accelerator, brake, or steering
  int kalHodnota = 662;
  int hodn;

  
void setup() {
  analogReference(INTERNAL);
  //Serial.begin(9600);

  
delay(5000);
  Keyboard.begin();

  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);
  pinMode(10, INPUT_PULLUP);
  pinMode(14, INPUT_PULLUP);
  pinMode(16, INPUT_PULLUP);

  Joystick.begin();
  Joystick.setXAxisRange(-12, 12);
  Joystick.setYAxisRange(-750, 300);
  delay(10000);

}

void loop() {
  if(kalHodnota==0 && millis()>2000){
    kalHodnota = analogRead(A0);
    }
    

tlacitka();

Joystick.setXAxis((analogRead(A0)-kalHodnota));


Joystick.setYAxis(-analogRead(A1));
//Serial.println(analogRead(A1));

}


void tlacitka(){
  if(digitalRead(2)){
  Joystick.setButton(0,0);
}else{
  Joystick.setButton(0,1);
}if(digitalRead(3)){
  Joystick.setButton(1,0);
}else{
  Joystick.setButton(1,1);
}if(digitalRead(4)){
  Joystick.setButton(2,0);
}else{
  Joystick.setButton(2,1);
}if(digitalRead(5)){
  Joystick.setButton(3,0);
}else{
  Joystick.setButton(3,1);
}if(digitalRead(7)){
  Joystick.setButton(4,0);
}else{
  Joystick.setButton(4,1);
}if(digitalRead(8)){
  Joystick.setButton(5,0);
}else{
  Joystick.setButton(5,1);
}if(digitalRead(9)){
  Joystick.setButton(6,0);
}else{
  Joystick.setButton(6,1);
}if(digitalRead(10)){
  Joystick.setButton(7,0);
}else{
  Joystick.setButton(7,1);
}if(digitalRead(14)){
  Joystick.setButton(8,0);
}else{
  Joystick.setButton(8,1);
}if(digitalRead(16)){
  Joystick.setButton(9,0);
}else{
  Joystick.setButton(9,1);
}

if(!digitalRead(6)){
 Keyboard.write(0xB1);
 }
}

// Miroslav Škoda 11.8.2018 verze 0.9B Arduino Leonardo